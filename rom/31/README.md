PC-8031 and similar external 5.25" disk units
=============================================

- `PC-8031-2W.bin`: PC-8031-2W IC25 2516 EPROM 2KB.
  - Sticker over window `004`; unit serial 2800249.
  - Same image as [jltursan].
- `PC-8031BE-1W.bin`: PC-8031BE-1W serial BE 1091019 (TA),
  - [this unit][8j-nec] as [posted here][8j-31].

### Hardware

- Power: AC 100V 50/60Hz 40W
- Drives: TEAC FD-50B-53-U
- Socketed ICs:
  - IC25   DIP-24 TI TMS2516JL EPROM (sticker says `004`)
  - IC38   DIP-40 NEC D780C-1: Z80 clone
  - IC46   DIP-40 NEC [D8255AC-5]: parallel I/F to computer
  - IC49   DIP-40 NEC [D765AC]: Floppy Disk Controller/Formatter
  - RAM1-8 DIP-16 8× NEC D416C-3 4116? DRAM

### Design

Memory Map:

    4000  7FFF 16K  DRAM    assumed from disassembly (stack ptr init $8000)
    0000  07FF  2K  ROM

I/O Map:

    port in/out
      FF    o   8255 control
      FE   i    8255 port C: b7-4 send to computer, b3-0 recv from computer
      FD    o   8255 port B: send to computer
      FC   i    8255 port A: recv from computer
      FB   io   (?) FDC data register (r/w according to datasheet)
      FA   i    (?) FDC status register (r-only according to datasheet)
      F8   io
      F7    o

8255 BSR mode parameters used in the code (writes to $FF) are:

    $09,$08     set,reset bit 4 (DAV)
    $0B,$0A     set,reset bit 5 (RFD)
    $0D,$0C     set,reset bit 6 (DAC)

D675 Register information:

- Status register (A0=0)
  - b0-3: `DnB` FDD #b busy seeking; FDC does not accept commands.
  - b4: `CB` FDC busy with read/write; does not accept commands.
  - b5: `EXM` Execution mode (only during non-DMA mode)
  - b6: `DIO` Data input/output, indicates data register direction
              (0=processor to FDC)
  - b7: `RQM` data register ready to send/receive data in `DIO` direction



<!-------------------------------------------------------------------->
[8j-31]: https://forum.vcfed.org/index.php?threads/searching-for-nec-pc-8001a-software-hardware.36037/post-871472
[8j-nec]: https://vintagecomputers.sdfeu.org/nec/
[D765AC]: ../../datasheet/D765-FDC.pdf
[D8255AC-5]: https://en.wikipedia.org/wiki/Intel_8255
[jltursan]:  https://forum.vcfed.org/index.php?threads/searching-for-nec-pc-8001a-software-hardware.36037/post-1340094
