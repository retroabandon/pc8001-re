#   Comments file for 31/PC-8031-2W.bin disassembly, in sed(1) -E form
#   Assumes you've run z80dasm with with -a.
#   https://www.gnu.org/software/sed/manual/sed.html

  /^savedreg_top-2/s//;&/

  /;0009/s//; `restart` sets to a RET instruction/

  /;0030/s//; rst 30h unused/

  /;004b/s//\n/
/\t\t;004e/s//; HL ↔ address at top of stack/
/\t;0053/s//; restore original HL/
/\t;0058/{
    s///
    s/\$7F42/savedreg_top+2/
}
/\t;005b/s//; $7F40/
/\t;005c/s//; $7F3E/
/\t;005d/s//; $7F3C/
/\t;005e/s//; $7F3A/
/\t;005f/s///
  /;0060/s///
/\t;0061/s//; $7F38/
/\t;0062/s//; $7F36/
/\t;0063/s//; $7F34/
/\t;0064/s//; $7F32/
  /;0065/s//; $7F30/
  /;0067/s//; $7F2E/
  /;0069/s//; interrupt page register/
/\t;006b/s//; $7F2C/

  /;007b/s//\n/

  /;0095/s//; RET instruction/

  /;0097/s//\n/
  /;009a/s//; clear some memory locations/
  /;00a4/s///
  /;00a7/s//; through $7F1F/
  /;00ab/s///
  /;00ad/s//; $FF/
  /;00ae/{s//\n/; a\
            ;   8255 PPI initialisation: $91 = %1.0010.001 = \
            ;   • b7   1= Set Input/Output mode \
            ;   • b65 00= Group A Mode 1 (strobed, C3,C4,C5 handshake) \
            ;   • b4   1= Port A input \
            ;   • b3   0= C upper (C7-4) output \
            ;   • b2   0= Group B Mode 0 (simple I/O) \
            ;   • b1   0= Port B output \
            ;   • b0   1= C lower (C3-0) input
}
  /;00b1/s///
  /;00b3/s//; PPI_ctrl/
  /;00b5/s//; PPI_C/
  /;00b7/s//; %0110 select DAC,RFD bits/
  /;00b9/s//; %0110 wait until both set/

  /;00bb/s//\n/
  /;00be/s//\n/

  /;00c1/s//; reset stack/
  /^_loop00CC/s/\t //
  /^_skip00E4/s/\t //
  /;00e6/s//\n/

  /;0195/s//\n/

  /;01a9/s//\n/

  /;02a3/s//\n/
  /^sub_02A4/s/\t//
  /^rst20sub/s/\t//
  /^_loop02A8/s/\t //
  /;02aa/s//; b7 = 1 ?/
  /;02ac/s//; b6 = 0 ?/
/\t;02ae/s//; wait until both are true/
/\t;02b0/s///
/\t;02b1/s//; A is $00/

  /;02b3/s//\n/

  /;02d4/s//\n/
  /;031a/s//\n/

  /;031c/s//\n/

  /;035a/s//\n/
  /;035b/s/\t//             # until we fix name
/\t;035b/s//; index?/
/\t;035c/s///
/\t;0360/s//; index * 2/
/\t;0361/s//; add base pointer LSB/
/\t;0362/s//; update pointer/
/\t;0363/s//; read pointer at (HL) into HL/
/\t;036[456]/s///
/\t;0367/s//; load value to which it points/

  /;0368/s//\n/
  /;036b/s//\n/

  /;0374/s//\n/

  /;0481/s//\n/

  /;05cd/s//\n/

/\t;05ea/s//; bottom of saved regs area/
  /;05ff/s//\n/

  /;0616/s//\n/
  /^sub_0617/s/\t//
/\t;0617/s//; above saved A register?/
  /;0627/s//\n/

  /;067f/s//\n/
  /;0690/s//\n/
  /;06a5/s//\n/

  /;06ae/{s//\n/; a\
;   Call rst10sub twice, returning the two A results in H and L
}

  /;06b3/s//\n/

/\t;062c/s//; top of saved regs area/

  /;06d8/s//\n/
  /^rst10sub/s/\t//
  /;06ef/{s///;a\
            ;
}
  /;0707/{s///;a\
            ;
}

  /;070e/s//\n/
  /^rst18sub/s/\t//

  /;073f/s//\n/

  /;07b9/s//\n/

  /;07c2/{s//\n/; a\
;   This may be dynamically used, or just rubbish from a previous build.
}
  /;07c5/s//\n/

  /;07ef/s//\n/
