PC-8xxx ROM Dumps
=================

Computers:
- `80/`: PC-8001 (original) ([NeoKobe][nk8001])
- `80ii/`: PC-8001mkII ([NeoKobe][nk8001])
- `88ii/`: PC-8801mkII ([TOSEC][to8801])
- `88sr/`: PC-8801mkII SR ([TOSEC][to8801])
- `88fr/`: PC-8801mkII SR ([TOSEC][to8801])

Peripherals:
- `31-2W`: PC-8031 and similar external 5.25" disk units

### Font ROMs

The following font ROM files are identical:
- `80/FONT80.bin`
- `80ii/FONT80M2.bin`


Disassembly
-----------

The `disassemble` script uses [`z80dasm`][] (available as a package on most
Linux distributions) and `sed` to produce `*/*.dis` from the following
files for each disassembly:

    header.z80      generic header comments and assembler setup
    common.sym      z80dasm symbol definitions common to all disassemblies
    */*.bin         the ROM image itself (sometimes renamed from *.rom)
    */info/*.header header specific to the disassembly for that ROM
    */info/*.block  z80dasm data (non-code) block definitions
    */info/*.sym    z80dasm symbol definitions for the particular disassembly
    */info/*.sed    adds comments and formatting to the dissassembled output

### Disassembly Notes

The `80ii/N80_2.bin` is 8 KB longer than the `80/N80_1*.bin` images, but
the first 24 KB is substantially the same. It appears that "N-BASIC" and
"N80-BASIC" share the same core and the switch on the back of the 8001mkII
simply changes how the BASIC configures itself. (XXX But this needs to
be confirmed.)

Note that the $6000-$7FFF range in N-80 BASIC is _not_ an expansion ROM;
it's normaly invisible (with the expansion ROM or unmapped memory) in that
address range; presumably N80-BASIC switches it in only when it needs it.

### Reverse-engineering To-do List

The "拡張ＲＯＭ" section of [[EnrPc]] explains about the expansion ROM "AB"
signature detection in the PC-8001, which is already disassembled. However
it mentions two other things that should be looked at:

1. If location $7FFF contains $55 (ASCII `U`), the the BASIC `MON` command
   will jump to $6002 instead of its usual routine.

2. Something about PC-8001mkII distinguishing between N80-BASIC ROM and
   expansion ROM by looking for $00 at $6002: ony if it's set to $00
   (`NOP`) then will $6002 will be executed as an expansion ROM.




<!-------------------------------------------------------------------->
[nk8001]: https://archive.org/details/Neo_Kobe_NEC_PC-8001_2016-02-25
[to8801]: https://archive.org/details/NEC_PC_8801_TOSEC_2012_04_23

[`z80dasm`]: https://linux.die.net/man/1/z80dasm

[EnrPc]: http://www43.tok2.com/home/cmpslv/Pc80/EnrPc.htm
