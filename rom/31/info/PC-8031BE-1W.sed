#   Comments file for 31/PC-8031BE-1W.bin disassembly, in sed(1) -E form
#   Assumes you've run z80dasm with with -a.
#   https://www.gnu.org/software/sed/manual/sed.html

# unused label
/^rst30/s//     /

/\t;004b/s//\n/

/\t;0085/s//; jump to loaded address \n/
  /^unkjmptbl/s/\t //
/\t;00a6/s//\n/
/\t;00ad/s//\n/

/\t;04b6/s//\n/
/^sub_04B7/{s/\t//;s/;04b7//}

/\t;04e9/s//\n/

/\t;07ef/s//\n/
