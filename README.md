PC-8001 / PC-8801 Reverse Engineering
=====================================

Contents:
- `datasheet/`: Consolidated datasheets for all hardware.
- `mod/`: System hardware modifications.
- `rom/`: Computer and peripheral ROM images.
- `sch/`: Schematics.
- `emu/`: Emulation notes and scripts.


Tape Format
-----------

The PC-8001 tape format detailed in [MZ→PC 変更プログラム][mzpc]
(_I/O Magazine,_ 1980-10), including Sharp MZ code to write PC-8001
-format tapes.

- Space bit is two cycles of 1200 Hz; mark bit is four cycles of 2400 Hz.
- Header block: `3A AA aa cc` load address upper/lower, checksum.
- Data block: `3A ll dd ... dd ck` data length, data, checksum.
- End block: `3A 00 00`.



<!-------------------------------------------------------------------->
[mzpc]: https://archive.org/details/Io198010/page/n108/mode/1up
