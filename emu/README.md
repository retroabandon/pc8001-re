PC-8001/8801 Emulation
======================

Emulators:
- Emulation General Wiki, [NEC PC-8800 series][emugen] gives a list of and
  links to more than a dozen different emulators.
- ePC-8801/mkII/MA, ePC-8001/mkII/SR from the [Common Source Code
  Project][cscp]. These are Win32 only.
- [XM8]: Derived from ePC-8x01 above. Windows/Linux/MacOS/Android.
- [MAME]: Multi-platform, with Windows binaries available from the [GitHub
  project][mame-gh]. Linux binaries are distributed by the distro vendors.
  Building from source takes a lot of disk space (>5 GB) and time.


Common Source Code Project
--------------------------

The [`cscp`](./cscp) script will start the CSCP `pc8001.exe` emulator under
Wine, first downloading and extracting it if necessary in
`.build/emu/cscp/` and setting up the ROM files in the same directory.

Details are found on the [PC-80/88/98 emulators page][cscp-pc98]. The
[binary archive][cscp-bin] can be downloaded from the [main page][cscp].

The binary archive contains a considerable number of `*.txt` files, but the
documentation in them isn't always complete. You can also refer to the
source code by cloning the [`mc68-net/takeda-cscp-emulators`][cscp-gh]
project from GitHub. (This is manually updated as new versions are
released.)

The emulators do not come with ROM images (even though some files, such as
`pc8801.txt`, mention "Internal ROM images"). ROM images with the correct
names appear to be found if you put them in the same directory as the
executable. Other search paths for ROMs have not yet been researched.

Note that the `pc8801.txt` does not mention how to deal with the font
ROM images. For `pc8001.exe` you need to put the font data in `FONT.ROM`
but offet by $1000 bytes from the start.

#### Linux/Wine

If running these under Wine on Linux, you may encounter the following
problems:
- `wine: could not load kernel32.dll, status c0000135`: This happens if you
  installed the `wine32:i386` package after having started the 64-bit
  `wine`. Remove your `~/.wine/` folder so that it will be set up again
  on the next run.


XM8
---

The retropc.net [ＰＩ． Home Page][pi] has links to emulators for X68000
(XM6), FM-7 (XM7) and PC8x01 ([XM8] and SDI88); these are apparently
derived from the Common Source Code Project emulators above, but run on
Windows, Linux, MacOS and Android.

There is a fork of XM8 on GitHub at [`nakatamaho/xm8m`][nt-xm8m]; it's not
clear what the difference is.


MAME
----

Use `mame pc8001` to start the PC-8001 emulator, or just `mame` to bring up
a selection window that lets you search all emulations. Other functions:
- `mame -ll` to list all "game" (system emulation) names.
- `mame -romident DIR`
- `mame -lr pc8001` to list internal hashes it wants for a system.

Options:
- `-rp PATHS`: list of colon-separated paths to find ROM or HDD images. The
  default path on Linux is `~/.mame/roms/`. See below for more on ROM paths.

Also see [How does MAME look for files?][mame-search].

### ROMs

Note that `BAD` and `BAD_DUMP` are merely marks from the MAME developers
that they want the ROM to be re-dumped; they do not affect operation except
to give a warning. If it doesn't run with `BAD_DUMP` ROMs, you have another
problem.

It appears that MAME requires very specific paths for ROM files; each path
given to the `-rp` option must have a subdirectory named for the device
(i.e., `pc8001` or `pc80s31`) and the ROM files must have the exact names
that MAME is expecting. Instead of a subdirectory, you may also have a ZIP
file with that name containing the ROMs.

The archive.org [mame-merged][ar-mm] item contains individiual ZIP files
with ROMs for each MAME platform ("game").

With MAME 0.261, placing `pc8001.zip` and `pc80s31.zip` in `~/mame/roms/`
let `mame pc8001` work. There appears to be no way to disable the emulated
disk drive.


<!-------------------------------------------------------------------->
[XM8]: http://retropc.net/pi/xm8/
[cscp]: http://takeda-toshiya.my.coocan.jp/
[mame]: https://www.mamedev.org/
[emugen]: https://emulation.gametechwiki.com/index.php/NEC_PC-8800_series
[mame-gh]: https:github.com/mamedev/mame

<!-- Common Source Code Project -->
[cscp-bin]: http://takeda-toshiya.my.coocan.jp/common/binary.7z
[cscp-gh]: https://github.com/mc68-net/takeda-cscp-emulators
[cscp-pc98]: http://takeda-toshiya.my.coocan.jp/pc9801/index.html

<!-- XM8 -->
[nt-xm8m]: https://github.com/nakatamaho/xm8m/
[pi-xm8]: http://retropc.net/pi/xm8/
[pi]: http://retropc.net/pi/

<!-- MAME -->
[ar-mm]: https://archive.org/download/mame-merged/mame-merged/
[mame-search]: https://docs.mamedev.org/usingmame/assetsearch.html
