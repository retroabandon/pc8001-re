Usagi Electric PC-8001 Modification
===================================

(Data here mainly from Usagi Electric Discord server.)

Currently it's not known what this modification does. However, it looks as
if it may be a mod that lets you set the bps rate jumper CN8 via software.

The standard jumper block for bps rate is ([[hb68]] p.91):

                                         ×16     ×64
             CN8                1-2     4800    1200
       ┌─────────────┐          1-3     2400     600
     1 │ ○ ○ ○ ○ ○ ○ │ 6        1-4     1200     300
       └─────────────┘          1-5      600     150
                                1-6      300      75


Components
----------

ICs on daughterboard are mainly [MC14001B series CMOS gates][on14gates],
which are pin-for-pin replacements of CD4000-series.
- __MC14011B:__ Quad 2-input NAND
  - `1A 1B 1Y 2Y 2A 2B Vss ‥ 3A 3B 3Y 4Y 4A 4B Vdd`
- __MC14013B:__ Dual Type D Flip-Flop
  - `1Q 1Q̅ 1C 1R 1D 1S Vss ‥ 2S 2D 2R 2C 2Q̅ 2Q Vdd`
- __MC14001B:__ Quad 2-input NOR
  - Same pinout as NAND.
- __MC14024B:__ 7-Stage Ripple Counter
  - `CLOCK RESET Q7 Q6 Q5 Q4 Vss ‥ NC Q3 NC Q2 Q1 NC Vdd`
  - All outputs low as long as RESET held high.
  - Falling clock edge advances one count.
- __TC4019BP:__ Dual D-Type Flip Flop
  - Same pinout as MC14013B.
  - This dates the board to 1985-02 (start of production) or later.
- __MC14049UB:__ Hex Inverter/Buffer
  - `Vdd 1Y 1A 2Y 2A 3Y 3A Vss ‥ 4A 4Y 5A 5Y NC 6A 6Y NC`

Also:
- Top: three resistors.
- Bottom: 1 KΩ resistor.
- Top: red cap?
- Bottom: 6× 103 (10 nF) caps; all appear to be IC bypass caps.

### References:

Daughterboard ICs:
- On Semiconductor, [MC 14001B Series B-Suffix Series CMOS Gates][on14gates].
  AND/OR/NAND/NOR.
- On Semiconductor, [MC 14013B Dual Type D Flip-Flop][on14013].
- On Semiconductor, [MC14024B: 7-Stage Ripple Counter][on14024].
- On Semiconductor, [MC14049B, MC14050B Hex Buffer][on14049].
- Toshiba, [TC4013BP, TC4013BF Dual D-Type Flip Flip][tc4019].



<!-------------------------------------------------------------------->
[hb68]: https://archive.org/stream/PC8001600100160011982#page/n5/mode/1up

[on14013]: https://www.onsemi.com/pdf/datasheet/mc14013b-d.pdf
[on14024]: https://www.onsemi.com/pdf/datasheet/mc14024b-d.pdf
[on14049]: https://www.onsemi.com/pdf/datasheet/mc14049b-d.pdf
[on14gates]: https://www.onsemi.com/pdf/datasheet/mc14001b-d.pdf
[tc4019]: https://datasheet.lcsc.com/lcsc/2107300130_TOSHIBA-TC4013BP-N-F_C1550122.pdf
