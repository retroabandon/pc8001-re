PC-8001 ROMs
============

Sources and Versions
--------------------

This directory contains the complete contents of the following files from
the [NeoKobe NEC PC-8001 2016-02-25][nk8001] archive:
- `[BIOS] PC-8001 [ROM].7z`: All `*.rom` files (renamed `*.bin` in this repo).
- `[BIOS] PC-8001 [extras].7z`: Contains only a readme; see below.

This is the contents of the `[BIOS] PC-8001 (readme).txt` from the extras
archive:

    PC-8001 System ROMs

    FONT80.ROM      Font ROM. Identical to FONT80M2.ROM and FONTKATA.ROM, and to first 2KB of FONT80SR.ROM.
    N80_101.ROM     N-BASIC 1.01
    N80_102.ROM     N-BASIC 1.02
    N80_11.ROM      N-BASIC 1.10

Additionally, the vcfed.org forum user [1980s_john has dumped][80be] his
PC-8001BE ROMs. Here are the original filenames from his archive and their
disposition here. (The original archive also contains Intel-format `.hex`
versions of each of these.)
- `CG BE.bin`: Renamed to `FONT80BE.bin` (character generator ROM)
- `BASIC 25[789].bin` Three files concatenated in their numerical order to
  produce `N80_11BE.bin`. The file is almost identical  to `N80_11.bin`,
  with only 4× $5C `\`/`¥` bytes replaced with 4× $24 `$` at locations
  $2D91, $2E53, $59B7 and $59C6.

### Genuine ROM Checks

- Serial 2062363 (awaiting check/dump):
  - IC10: D2364C-257 NBASIC P23689-212
  - IC11: D2364C-073 NBASIC P23609-212
  - IC12: D2364C-260 NBASIC P23869-212

<!-------------------------------------------------------------------->
[nk8001]: https://archive.org/details/Neo_Kobe_NEC_PC-8001_2016-02-25
[80be]: https://forum.vcfed.org/index.php?threads/searching-for-nec-pc-8001a-software-hardware.36037/post-1354958
