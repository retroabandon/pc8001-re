Schematics
==========

Contents:
- Systems
  - PC-8001
- Accessories and Peripherals
  - Kanji Boards
  - PC-8031 Floppy Disk Subsystem
  - PC-8001 and PC-8001mkII CMT SD-Card Adapter

Generally I/O magazine is a good place to look for schematics; they often
publish a "全回路図" article a few months after a major new system is
released and republish them in their I/O別冊 books. There is an [collection
of I/O ToC pages][IO-toc] available, and archive.org has full scans of
[most issues][IO-ar]. (You can't search for a slash, so search for, e.g.,
"アイ・オー 1980年12月号"


Systems
-------

### PC-8001

`pc-8001-sch-IO別冊.pdf` contains the schematics republished (with
corrections) in I/O別冊 [PC-8001活用研究 p.333]. They were originally
published in I/O magazine [1980年12月号 p.209][I/O 80-12 p.209], with a
minor correction (missing connection between IC 9 pin 4 and IC47 pin 13 on
p.210) in [1981年02月号 p.152][I/O 81-02 p.152].


Accessories and Peripherals
---------------------------

### PC-8001 Kanji Boards

I/O別冊 [PC活用研究 8001/8001mkII/8801 p.305][PC活用研究 p.305], as a
description of using a Sharp MK kanji board on a PC-8000/8800 system. No
schematic is provided, but there are enough details to work out generally
how to connect kanji ROMs. The ROMs used appear to be similar to the ones
on also used on the Epson MP-80K printer.

### PC-8031 Floppy Disk Subsystem

The PC-8031 (single-sided drives) and PC-8031-2W (double-sided drives)
differ only in the drives themselves and the ROM. I/O magazine
[1985年09月号 p.194][IO 85-09 p.194] has an article on this. You can copy
an 80S31 ROM and make the following changes:

      address  80S31 -2W
       0161      DA   3A
       0183      96   AA
       01E8      D6   AA
       01E9      07   01
       024B      CD   FB
       024C      C6   76
       024D      07   F3
       02DB      D6   AA
       02DC      07   01
       0397      D6   AA
       0398      07   01
       03C4      CD   FB
       03C5      C6   76
       03C6      07   F3
    07C6〜07EE   ??   00

### PC-8001 and PC-8001mkII CMT SD-Card Adapter

`PC-8001_SD.pdf` and `PC-8001mk2_SD.pdf` are from recent projects by
yanataka60, [PC-8001_SD] and [PC-8001mk2_SD], that load `.CAS` files from
an SD card. These are primarily useful as documentation of the expansion
connectors and examples of how to interface an 8255 PPI to either system
and a 2764 ROM to the PC-8001.

The use of `R̅O̅M̅D̅I̅S̅3` in the PC-8001 version is particulary interesting;
it looks as if it's used as both an input and an output signal.



<!-------------------------------------------------------------------->
[IO-toc]: http://io40th.kohgakusha.co.jp/2017/09/3440io40.html
[IO-ar]: https://archive.org/details/iomagazine

[I/O 80-12 p.209]: https://archive.org/details/io-198012/page/209/mode/1up
[I/O 81-02 p.152]: https://archive.org/details/IO198102/page/n152/mode/1up
[PC-8001活用研究 p.333]: https://archive.org/details/pc-8001_202108/page/n333/mode/1up

[PC活用研究 p.305]: https://archive.org/details/IoPc80018001mkii8801/page/n305/mode/1up

[IO 85-09 p.194]: https://archive.org/details/Io19859/page/n195/mode/1up

[PC-8001_SD]: https://github.com/yanataka60/PC-8001_SD/
[PC-8001mk2_SD]: https://github.com/yanataka60/PC-8001mk2_SD
