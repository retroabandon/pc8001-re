#   Comments file for 80/N80_101.rom disassembly, in sed(1) -E form
#   Assumes you've run z80dasm with with -a.
#   https://www.gnu.org/software/sed/manual/sed.html

s/^(jp_unk006A)\t  /\1/
s/\t;0013/; why not just use rst1?/

s/\t;0035/;0035\n/
s/;0038/&\n/

s/\t;003b//
s/\t;003c/; clear keyboard buffer?/
s/\t;003f/; STOP or ESC+STOP pressed?/
s/\t;0042/;   no: carry on with regular init/

  /^jp_hotstart/s/\t   //

/^hotstart/s/\t//
s/\t;0069/\n/
s/\t;006[abe]//
s/\t;006f/; turn off fkey display/
s/\t;0072//
s/\t;0073/; set display to color mode/
s/;007e/\n/

####################################################################

  /;0663/s//\n/
  /^sub_0664/s/\t//
  /;0667/s/\$19.*/25/

  /;0674/{s//\n/;a\
;   Start adddress of each line of text on the CRT. [m.21]
}
  /;06a7/s//\n/

####################################################################

/;08f5/{ s/;08f5//; a\
            ; fallthrough\
\
;   ♠BC ♣AFDE Set line 25 function key display and color or BW mode. [m.33]\
;   • B: $FF=fkey display on; $00=off \
;   • C: $FF=color mode;      $00=monochrome mode
}
s/;08f5/\n/
/^setFKCBW1\t/s/\t //
/^setFKCBW\t/s/\t//
/\t;08f6/s//; alternate entry point using BC from stack/
/;0914/s//\n/

####################################################################

/;094a/s//\n/

####################################################################

  /;0ace/s//\n/
  /^sub_0ACF/s/\t//

/\t;0b18/s//; [m.45]/

  /;0b2d/s//\n/
  /^sub_0B2E/s/\t//

####################################################################

/;0cf0/ { s//\n/; a\
;   ♠F ♣A ♡BCHL  STOP key/ESC key check.\
;   • Z=1: Neither STOP nor ESC is pressed.\
;   • C=1: STOP is pressed.
}
/^stopcheck\t/s/\t //
  /;0cf[123]/s///
/\t;0cf6/s//; read keyboard row 9/
  /;0cf8/s//; complement to get 1=pressed, 0=released/
  /;0cf9/s///
  /;0cfd/s//; mask out all but b7=ESC, b0=STOP keys/
/\t;0cff/s//; if neither pressed, ret with Z=1/
/\t;0d01/s//; save ESC+STOP status/
/\t;0d02/s//; add in any other pressed keys/
/\t;0d03/s//; save them all/
/\t;0d04/s//; restore ESC+STOP status/
/\t;0d0[56]/s///
/\t;0d07/s//; Z=1 if we got here from jr z above/
/\t;0d08/s//; b0=STOP moved to b7, copied to carry/
/\t;0d09/s//; if STOP not pressed, go ???/
/\t;0d0b/s//; ???/
  /;0d0d/s//; ret with C=1\n            ;/
  /;0d0e/s//; mask out all but shifted ESC key/
/\t;0d10/s//; if it's not set, return with Z=0/
  /;0d11/s//; ???/
  /;0d13/s//\n/


/^sub_0d14h/s/\t //

####################################################################

  /^sub_10A1/s/\t//
  /;10e4/s//\n/
  /;10b4/s/jp_hotstart.*/$13/

####################################################################
#   clear_keyups

/;107f/s//\n/
/;1098/{s///;a\
            ; fallthrough\
\
;   Clear keys no longer pressed in a given row. (This does not set any     \
;   pressed keys; that must be done afterwards by the caller.)              \
;   • HL: Pointer to keyboard row in `curkeys`                              \
;   •  A: bitfield: 0=key up, 1=key down                                    \
;   •  B: bitfield: same as A, or mask of some sort?                        \
;                                                                           \
;   The logic is as follows:                                                \
;                                                                           \
;       A M     ¬     ∧     ⊻       M                                       \
;       ─────────────────────────────                                       \
;       0 0   → 1 0 → 0 0 → 0 0 →   0   not now, not previously pressed     \
;       0 1   → 1 1 → 1 1 → 0 1 →   0   not now but previously pressed      \
;       1 0   → 0 0 → 0 0 → 0 0 →   0   pressed now but not previously      \
;       1 1   → 0 1 → 0 1 → 1 1 →   1   pressed now and previously          \
;                                                                           \
;   • If a key is up (0 bit in A), the same bit in M (memory, the keyboard  \
;     row byte to which HL points) is always cleared.                       \
;   • If a key is down (1 bit in A), M left in its previous state. (It is   \
;     _not_ set; that must be done by the caller if it wishes.)             \
;                                                                           \
;   XXX need to work out what the return value in A is.                     \
;
}
/^clear_keyups/s/\t   //
/\t;109a/s///
/\t;109b/s///
/\t;109c/s///
/\t;109d/s//; store new value without released keys/
  /;10a0/s//\n/

####################################################################
#   key2chr

  /;10e5/s//; [m.60]/
  /;117c/s//\n/

####################################################################
#   init

  /;1756/{s///;a\
\
; ----------------------------------------------------------------------\
;   ▶$1757 N-BASIC initialization.\
;
}
  /;1757/s//; touch all four 16K banks; why?/
  /;175[ad]/s///
  /;1760/s///
  /;1763/s//; ▶$EA00 RAM jump table of overridable/
/\t;1766/s//;   ROM routines/
  /;1769/s/val0157/$157/
  /;176[9c]/s///
/\t;17d5/s//\n/

/\t;17d8/s//; start of RAM on 32K system/
/\t;17db/s///
/\t;17dc/s//; write $00/
/\t;17dd/s//; read is $00?/
/\t;17de/s//;    no: assume 16K system/
  /;17e0/s//; A ← $FF/
/\t;17e1/s//; write $FF/
/\t;17e2/s//; read is $FF?/
/\t;17e3/{s//;   yes: it's a 32K system/; a\
            ; fallthrough       ;   no: it's a 16K system\

}

  /;1784/s//\n/

  /;1793/s//\n/

  /;179c/s//\n/

/\t;179e/s//; ??? check for DOS boot?\n/

/\t;17a1/s//; expansion ROM location (factory empty socket)/
/\t;17a4/s//; check for expansion ROM signature: "AB"/
  /;17a5/s/\$41.*/'A'/
  /;17a7/s///
/\t;17a8/s//; if not 'A', don't call expansion ROM/
  /;17aa/s/\$42.*/'B'/
/\t;17ac/s//; signature match: call ROM\n/

  /^set_txttb16/s/\t   //
/\t;17e5/s//; set TXTTAB to HL + 16K/
  /;17e8/s///
  /^set_txttab/s/\t  //
/\t;17e9/s//; ▶$17E9 set TXTTAB to HL (and init stuff?)/
/\t;17f0/s/jp_init\+1.*/12/

####################################################################
#   messages

  /;1837/{ s//&\n/; a\
;   Title message printed at startup:                                       \
;     NEC PC-8001 BASIC Ver 1.0                                             \
;     Copyright 1979 (C) by Microsoft                                       \
;
}
  /^msg_title/s/\t //

####################################################################
#   err_notimpl

  /;1874/{s//\n/; a\
;   ▶$1875 Unimplemented function error: prints "Disk BASIC Feature".
}
  /;1877/s//\n/

####################################################################
#   CLOAD messages

  /;1fc6/s//\n/
  /;1fc9/s//; "Bad" CR LF $00/
  /;1fc[abcd]/s///
  /;1fce/s//\n/

  /;1fee/s//\n/
  /^msg_found/s/\t //
  /;1fef/s//; "Found:" $00/
  /;1ff[0-5]/s///
  /^msg_skip/s/\t//
  /;1ff6/s//; "Skip:" $00/
  /;1ff[7-9a]/s///
  /;1ffb/s//\n/

####################################################################
#   base conversion

  /;3147/s//\n/
  /^bconvtbl/s/\t//
  /;3151/s//\n/

####################################################################
#   machine-language monitor

  /;5c6d/s//&\n/
  /^moncmdtbl/s/\t //

####################################################################

/\t;5fd6/s//;5fd6\n/
  /;5ff1/s//&\n/
  /^sub_5FF2/s/\t//

####################################################################
#   V1.1 new routines

  /;5fe5/{s//\n/; a\
;   Replaces `stopcheck` at two calls in V1.1 code.
}
/^stopcheck11\t/s/\t   //

  /;5fea/s//\n/

